import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;

public class PersonTest {

    private Person person;

    @Before
    public void setup(){
        person = new Person("Lukasz",26);
    }

    @Test
    public void testAdoptKitty(){
        person.adoptKitty("lotta");
        int rezult = person.getCatsNames().size();
        assertThat(rezult).isEqualTo(1);
    }
}
