import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import static java.util.Comparator.comparing;

public class Person {

    private String name;
    private int age;
    private List<String> catsNames;
    private int id;


    public int idGenerator() {
        Random random = new Random();
        int generatedId = random.nextInt(2000) + 1000;
        return generatedId;
    }


    public Person(String name, int age) {
        this.name = name;
        this.age = age;
        this.catsNames = new ArrayList<>();
        this.id = idGenerator();

    }

    public String catWithTheLongestName(){
        List<String> temp = new ArrayList<>(getCatsNames());
        temp.sort((o1, o2) -> {
            if (o1.length()<o2.length()){
                return 1;
            }else if (o1.length()>o2.length()){
                return -1;

            }else {
                return o1.compareTo(o2);
            }
        });
        System.out.println("Here are "+getName()+"'s cats:" );
        temp.forEach(System.out::println);
        System.out.println();
        System.out.println("It looks like " +temp.get(0)+ " has the longest name");
        return temp.get(0);
    }

    public void adoptKitty(String catName) {
        catsNames.add(catName);
    }

    public void printSortedKitties() {
        List<String> temp = new ArrayList<>(getCatsNames());
        temp.sort(String::compareTo);
        temp.forEach(System.out::println);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<String> getCatsNames() {
        return catsNames;
    }

    public void setCatsNames(List<String> catsNames) {
        this.catsNames = catsNames;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String catsNamesAsString(){
        StringBuilder sb = new StringBuilder();
        String kitties=null;
        for (int i = 0; i <getCatsNames().size() ; i++) {
            kitties = sb.append(getCatsNames().get(i)).append(" \n").toString();
        }
        return kitties;

    }

    @Override
    public String toString() {


        return "This is " + name + ". " +
                name + " is " + age+ " and has id of "+
                id+". "+name +" loves cats. Has: \n"+ catsNamesAsString();


    }
}
